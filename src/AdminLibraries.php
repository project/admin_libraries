<?php

namespace Drupal\admin_libraries;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Theme\ThemeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Main service class for Admin Libraries.
 * 
 * @package Drupal\admin_libraries
 */
class AdminLibraries implements ContainerInjectionInterface {

  /**
   * The module extension list.
   *
   * @var Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;
  
  /**
   * The theme extension list.
   *
   * @var Drupal\Core\Extension\ThemeExtensionList
   */
  protected $themeExtensionList;

  /**
   * The theme manager.
   *
   * @var Drupal\Core\Theme\ThemeManager
   */
  protected $themeManager;

  /**
   * Constructs a new AdminLibraries object.
   * 
   * @param Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list.
   * @param Drupal\Core\Extension\ThemeExtensionList $themeExtensionList
   *   The theme extension list.
   * @param Drupal\Core\Theme\ThemeManager $themeManager
   *   The theme manager.
   */
  public function __construct(
    ModuleExtensionList $moduleExtensionList,
    ThemeExtensionList $themeExtensionList,
    ThemeManager $themeManager,
  ) {
    $this->moduleExtensionList = $moduleExtensionList;
    $this->themeExtensionList = $themeExtensionList;
    $this->themeManager = $themeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module'),
      $container->get('extension.list.theme'),
      $container->get('theme.manager'),
    );
  }

  /**
   * Implements hook_page_attachments().
   */
  public function pageAttachments(array &$attachments) {
    $module_infos = $this->moduleExtensionList->getAllInstalledInfo();
    $theme_infos = $this->themeExtensionList->getAllInstalledInfo();
    $extension_infos = array_merge($module_infos, $theme_infos);
    $active_theme = $this->themeManager->getActiveTheme()->getName();

    foreach ($extension_infos as $extension_info) {
      if (!array_key_exists('admin_libraries', $extension_info)) {
        continue;
      }

      if (!array_key_exists($active_theme, $extension_info['admin_libraries'])) {
        continue;
      }

      foreach ($extension_info['admin_libraries'][$active_theme] as $library) {
        $attachments['#attached']['library'][] = $library;
      }
    }
  }
}
